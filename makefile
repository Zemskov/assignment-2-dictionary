ASM=nasm
LD=ld
ASMFLAGS=-f elf64

all: main

main: lib.o main.o dict.o 
	$(LD) -o main $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<


main.o: main.asm colon.inc words.inc lib.inc dict.inc

lib.o: lib.asm 

dict.o: dict.asm lib.inc

.PHONY: test clean 

clean:
	rm *.o main

test:
    python3 test.py
