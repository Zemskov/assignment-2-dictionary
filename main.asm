%include "lib.inc"
%include "dict.inc"

%define LIMIT 256
%define ERROR 1

section .buf
buffer: resb LIMIT

section .errors
buff_size_err: db "Buffer size is not enough. Only 255 bytes available.", 10, 0
error_key: db "key not found", 10, 0

%include "words.inc"


section .text

global _start
_start:
	mov rdi, buffer
	mov rsi, LIMIT
	call read_word
	test rax, rax
	je .buffer_err

	mov rsi, ptr
	mov rdi, buffer
	call find_word
	test rax, rax
	je .key_err

	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	jmp exit

.key_err:
	mov rdi, error_key
	call print_error
	mov rdi, ERROR
	jmp exit
.buffer_err:
	mov rdi, buff_size_err
	call print_error
	mov rdi, ERROR
	jmp exit
