from subprocess import PIPE, Popen

err = "key not found"

tests = {
    "test1" : 1,
    "test2" : 2,
    "test3" : 3,
    "" : err
}



def test(word):
    process = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate(word)
    out=out.decode().strip()
    err=err.decode().strip()
    if (out != "" and out = tests[word]):
        print(f"Input: {word}\nOutput: {out},\nExpected: {tests[word]}")
        return False
    elif (err != "" and err != err):
        print(f"Input: {word}\nOutput error: {err}\nExpected error: {tests[word]}")
        return False
    return True
    



if (test("test1") and test("test2") and test("test3") and test("fdsfds")):
    print("OK")
