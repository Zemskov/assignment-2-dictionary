%include "lib.inc"

section .text
global find_word
find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

loop:

    add r13, 8            ; move rsi to key word address
    call string_equals    ; compare word from dict with input word
    sub r13, 8
    test rax, rax          ; if find_word -> return word
    jnz  .end
    mov r13, [r13]        ; put in rsi next element of list
    test r13, r13
    jnz loop      ; if end of list -> return 0, otherwise check next
    xor rax, rax
    pop r13
    pop r12
    ret
    .end:
        mov rax, r13
        pop r13
        pop r12
        ret
